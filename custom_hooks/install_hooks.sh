#!/bin/bash

SOURCE="${BASH_SOURCE[0]}"

chmod +x $( dirname "$SOURCE" )/pre-commit
chmod +x $( dirname "$SOURCE" )/prepare-commit-msg


ln -sf ../../custom_hooks/pre-commit $( dirname "$SOURCE" )/../.git/hooks/pre-commit
ln -sf ../../custom_hooks/prepare-commit-msg $( dirname "$SOURCE" )/../.git/hooks/prepare-commit-msg
